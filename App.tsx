import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  getForegroundPermissionsAsync,
  requestBackgroundPermissionsAsync,
  requestForegroundPermissionsAsync,
  startGeofencingAsync,
} from "expo-location";
import * as Location from "expo-location";

import * as Notifications from "expo-notifications";
import { LocationGeofencingEventType } from "expo-location";
import * as TaskManager from "expo-task-manager";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

TaskManager.defineTask(
  "GEOFENCE_TASK",
  ({ data: { eventType, region }, error }) => {
    if (error) {
      // check `error.message` for more details.
      return;
    }
    if (eventType === LocationGeofencingEventType.Enter) {
      console.log("You've entered region:", region);
      Notifications.scheduleNotificationAsync({
        content: {
          title: "ENTERED GEOFENCE",
          body: region.identifier,
        },
        trigger: null,
      });
    } else if (eventType === LocationGeofencingEventType.Exit) {
      console.log("You've left region:", region);
      Notifications.scheduleNotificationAsync({
        content: {
          title: "EXITED GEOFENCE",
          body: region.identifier,
        },
        trigger: null,
      });
    }
  }
);

export default function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [location, setLocation] = useState<Location.LocationObject>();
  useEffect(() => {
    const setUp = async () => {
      const { granted: notificationsGranted } =
        await Notifications.getPermissionsAsync();
      if (!notificationsGranted) {
        await Notifications.requestPermissionsAsync();
      }
      const { granted: fgGranted } = await getForegroundPermissionsAsync();

      if (!fgGranted) {
        await requestForegroundPermissionsAsync();
        await requestBackgroundPermissionsAsync();
      }
      GetLocation();
      const geofences = [
        {
          identifier: "Muradnagar",
          latitude: 28.771646,
          longitude: 77.507561,
          radius: 1000,
          notifyOnEnter: true,
          notifyOnExit: true,
        },
      ];
      await startGeofencingAsync("GEOFENCE_TASK", geofences);
    };

    setUp();
  }, []);

  const GetLocation = async () => {
    const { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      console.log("PERMISSION LACK!");
    }
    console.log("location");
    const userLocation = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.Highest,
    });
    setLocation(userLocation);
    setIsLoading(false);
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <Text>App is Loading</Text>
      ) : (
        <Text>{JSON.stringify(location)}</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
